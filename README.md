![logo-smartonline](https://www.enviabybus.com.br/assets/images/logo-ebb-white.svg)

## Desafio para vaga de desenvolvedor backend

Por favor, leia este documento do começo ao fim, com muita atenção. O objetivo deste teste é avaliar seus conhecimentos técnicos em programação.

O desafio pode ser dividido em várias fases, começando com a manipulação de arquivo e terminando com uma API funcional. Cada fase testa diferentes habilidades que você precisará como desenvolvedor backend.

### Fase 1: Manipulação de Arquivo e Banco de Dados

Sua primeira tarefa é parsear este arquivo de texto [CNAB](https://gitlab.com/enviabybus/desafio-backend-cnab/-/raw/main/CNAB.txt) e salvar suas informações (_transações financeiras_) em um banco de dados de sua escolha.

### Fase 2: Criação de API ou Página Web

Após a manipulação do arquivo e o armazenamento dos dados, você deve criar uma API ou página web que permita o upload de um novo arquivo CNAB, normalize os dados e retorne as informações.

---

## Requisitos da Aplicação Web:

- **Framework**: Use um framework bem conhecido no mercado.
- **Upload de Arquivo**: Ofereça um meio (tela ou API) para o upload do arquivo CNAB.
- **Processamento de Arquivo**: Leia e normalize os dados do arquivo CNAB e salve-os em um banco de dados apropriado.
- **Validação de CPF**: Certifique-se de que os CPFs são válidos. Transações com CPFs inválidos devem ser sinalizadas.
- **Lista de Operações**: Mostre uma lista de transações importadas, agrupadas por lojas, com um totalizador de saldo.
- **Transações com Erro**: Apresente uma lista de transações que falharam no processamento.
- **Configuração e Execução**: O sistema deve ser fácil de configurar e executar, e deve utilizar apenas tecnologias livres ou gratuitas.
- **Versionamento**: Utilize Git, com commits claros e descritivos.
- **Banco de Dados**: Você pode usar PostgreSQL, MySQL, SQL Server ou MongoDB.
- **Documentação**: Inclua um arquivo README bem escrito que explique como configurar e rodar o projeto.
- **Consumo da API**: Forneça instruções básicas para a interação com a API, se houver.
- **Erros de Upload**: Se o upload do arquivo falhar, o usuário deve ser notificado.
- **Erros de Banco de Dados**: Trate qualquer falha de armazenamento de maneira elegante e informe o usuário.

**Você ganhará pontos extras se:**

- Utilizar uma linguagem tipada, especialmente se for Node.JS com Typescript.
- Documentar sua API.
- Desenvolver testes automatizados.
- Utilizar Docker Compose.

## O que será avaliado

- Domínio das linguagens utilizadas
- Organização
- Coesão e design de código
- Lógica
- Preocupação com o resultado final
- Facilidade de configuração da aplicação
- Documentação
- Tratamento de erros

## Dicas

- **Testes de API**: Recomendamos o uso de [Postman](https://www.postman.com/downloads/) ou [Insomnia](https://insomnia.rest/download/) para testar sua API. Essas ferramentas são excelentes para simular requisições GET, POST, PUT e DELETE, permitindo que você valide o comportamento da sua aplicação de forma eficaz.

- **Documentação**: Ler e entender documentações é uma habilidade fundamental para qualquer desenvolvedor. Portanto, sempre consulte a documentação das ferramentas, frameworks e bibliotecas que você decidir utilizar.

---

Boa sorte!
